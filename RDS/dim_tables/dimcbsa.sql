CREATE TABLE edw.dimcbsa (
	cbsapk serial NOT NULL,
	cbsabk int4 NOT NULL,
	csabk int4,
	stateprovincefk int4 NOT NULL,
	countryfk int4 NOT NULL,
	sourcefk int4 NOT NULL,
	cbsaname varchar(100) NOT NULL,
	cbsatype varchar(50) NOT NULL,
	stateprovinceabbreviation varchar(20) NOT NULL,
	countryname varchar(50) NOT NULL,
	auditcreatedby varchar(45) NOT NULL,
	auditcreateddate timestamp NOT NULL,
	auditlastmodifiedby varchar(45) NOT NULL,
	auditlastmodifieddate timestamp NOT NULL,
	auditsoftdelete varchar(3) NOT NULL,
	PRIMARY KEY (cbsapk)
);
