CREATE TABLE edw.dimpatientstatus (
	patientstatuspk serial NOT NULL,
	patientstatusbk varchar(20) NOT NULL,
	sourcefk int4 NOT NULL,
	auditcreatedby varchar(45) NOT NULL,
	auditcreateddate timestamp NOT NULL,
	auditlastmodifiedby varchar(45) NOT NULL,
	auditlastmodifieddate timestamp NOT NULL,
	auditsoftdelete varchar(3) NOT NULL,
	PRIMARY KEY (patientstatuspk)
);
