CREATE TABLE edw.dimorganization (
	organizationpk serial NOT NULL,
	organizationbk varchar(45) NOT NULL,
	organizationaddressfk int4,
	parentorganizationfk int4,
	primarycontactfk int4,
	primarycontactbk varchar(20),
	sourcefk int4 NOT NULL,
	organizationname varchar(100) NOT NULL,
	federalidnumber varchar(45),
	dunandbradstreetnumber varchar(45),
	auditcreatedby varchar(45) NOT NULL,
	auditcreateddate timestamp NOT NULL,
	auditlastmodifiedby varchar(45) NOT NULL,
	auditlastmodifieddate timestamp NOT NULL,
	auditactiveindicator varchar(3) NOT NULL,
	auditsoftdelete varchar(3) NOT NULL
);
