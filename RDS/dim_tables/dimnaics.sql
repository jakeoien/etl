CREATE TABLE edw.dimnaics (
	naicspk serial NOT NULL,
	naicsbk varchar(10) NOT NULL,
	toplevelnaicsfk int4,
	toplevelnaicsbk varchar(10),
	parentnaicsfk int4,
	parentnaicsbk varchar(10),
	sourcefk int4 NOT NULL,
	naicsdescription varchar(100) NOT NULL,
	naicscategoryindicator varchar(3) NOT NULL,
	auditcreatedby varchar(45) NOT NULL,
	auditcreateddate timestamp NOT NULL,
	auditlastmodifiedby varchar(45) NOT NULL,
	auditlastmodifieddate timestamp NOT NULL,
	auditsoftdelete varchar(3) NOT NULL,
	PRIMARY KEY (naicspk)
);
