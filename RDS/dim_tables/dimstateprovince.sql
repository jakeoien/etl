CREATE TABLE edw.dimstateprovince (
	stateprovincepk serial NOT NULL,
	stateprovincebk varchar(20) NOT NULL,
	countryfk int4 NOT NULL,
	sourcefk int4 NOT NULL,
	stateprovincename varchar(50) NOT NULL,
	countryname varchar(50) NOT NULL,
	auditcreatedby varchar(45) NOT NULL,
	auditcreateddate timestamp NOT NULL,
	auditlastmodifiedby varchar(45) NOT NULL,
	auditlastmodifieddate timestamp NOT NULL,
	auditsoftdelete varchar(3) NOT NULL,
	PRIMARY KEY (stateprovincepk)
);
