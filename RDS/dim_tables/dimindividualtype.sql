CREATE TABLE edw.dimindividualtype (
	individualtypepk serial NOT NULL,
	individualtypebk varchar(45) NOT NULL,
	sourcefk int4 NOT NULL,
	individualtypedescription varchar(45) NOT NULL,
	auditcreatedby varchar(45) NOT NULL,
	auditcreateddate timestamp NOT NULL,
	auditlastmodifiedby varchar(45) NOT NULL,
	auditlastmodifieddate timestamp NOT NULL,
	auditsoftdelete varchar(3) NOT NULL
);
