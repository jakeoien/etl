CREATE TABLE edw.dimdiagnosis (
	diagnosispk serial NOT NULL,
	diagnosisbk varchar(45) NOT NULL,
	sourcefk int4 NOT NULL,
	diagnosisclassification varchar(100) NOT NULL,
	diagnosisdescription varchar(100) NOT NULL,
	auditcreatedby varchar(45) NOT NULL,
	auditcreateddate timestamp NOT NULL,
	auditlastmodifiedby varchar(45) NOT NULL,
	auditlastmodifieddate timestamp NOT NULL,
	auditsoftdelete varchar(3) NOT NULL,
	PRIMARY KEY (diagnosispk)
);
