CREATE TABLE edw.dimpayer (
	payerpk serial NOT NULL,
	payerbk varchar(45) NOT NULL,
	payertypefk int4,
	payertypebk varchar(20) NOT NULL,
	sourcefk int4 NOT NULL,
	payerdescription varchar(50),
	payergroup varchar(30),
	payercontactname varchar(100),
	payercontactphone varchar(25),
	payercontactemail varchar(100),
	auditcreatedby varchar(45) NOT NULL,
	auditcreateddate timestamp NOT NULL,
	auditlastmodifiedby varchar(45) NOT NULL,
	auditlastmodifieddate timestamp NOT NULL,
	auditsoftdelete varchar(3) NOT NULL
);
