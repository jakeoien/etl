CREATE TABLE edw.dimpatienthistory (
	patienthistorypk serial NOT NULL,
	priorpatienthistoryfk int4,
	patientpk int4 NOT NULL,
	patientbk varchar(45) NOT NULL,
	patientaddressfk int4,
	patientdobfk int4,
	sourcefk int4 NOT NULL,
	patientstatusfk int4,
	patientstatusbk varchar(20),
	patientdob date,
	patientage int4,
	patientfirstname varchar(30),
	patientmiddlename varchar(30),
	patientlastname varchar(35) NOT NULL,
	patientssn varchar(20),
	patientprefix varchar(35),
	patientsuffix varchar(35),
	patientname varchar(180) NOT NULL,
	patientgender varchar(20),
	patientheight numeric(10,1),
	patientweight numeric(10,1),
	patientethnicity1 varchar(45),
	patientethnicity2 varchar(45),
	patientethnicity3 varchar(45),
	patientethnicity4 varchar(45),
	patientreligion varchar(45),
	patientfirstlanguage varchar(45),
	patientveteranstatus varchar(3),
	patienthomephonenumber varchar(45),
	patientcellphonenumber varchar(45),
	auditeffectivestartdate timestamp NOT NULL,
	auditeffectiveenddate timestamp,
	auditcreatedby varchar(45) NOT NULL,
	auditcreateddate timestamp NOT NULL,
	auditlastmodifiedby varchar(45) NOT NULL,
	auditlastmodifieddate timestamp NOT NULL,
	auditcurrentrowidentifier varchar(50) NOT NULL,
	auditsoftdelete varchar(3) NOT NULL,
	PRIMARY KEY (patienthistorypk)
);
