CREATE TABLE edw.dimcountry (
	countrypk serial NOT NULL,
	countrybk varchar(20),
	sourcefk int4 NOT NULL,
	countryname varchar(50) NOT NULL,
	countryabbreviation varchar(10),
	countryabbreviationalternate varchar(10),
	auditcreatedby varchar(45) NOT NULL,
	auditcreateddate timestamp NOT NULL,
	auditlastmodifiedby varchar(45) NOT NULL,
	auditlastmodifieddate timestamp NOT NULL,
	auditsoftdelete varchar(3) NOT NULL,
	PRIMARY KEY (countrypk)
);
