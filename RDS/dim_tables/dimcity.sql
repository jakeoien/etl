CREATE TABLE edw.dimcity (
	citypk serial NOT NULL,
	cbsafk int4 NOT NULL,
	stateprovincefk int4 NOT NULL,
	sourcefk int4 NOT NULL,
	cityname varchar(50) NOT NULL,
	auditcreatedby varchar(45) NOT NULL,
	auditcreateddate timestamp NOT NULL,
	auditlastmodifiedby varchar(45) NOT NULL,
	auditlastmodifieddate timestamp NOT NULL,
	auditsoftdelete varchar(3) NOT NULL,
	PRIMARY KEY (citypk)
);
