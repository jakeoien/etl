CREATE TABLE edw.dimlevelofcare (
	levelofcarepk serial NOT NULL,
	levelofcarebk varchar(45) NOT NULL,
	paymentrevenuecode varchar(15),
	sourcefk int4 NOT NULL,
	levelofcaredescription varchar(45) NOT NULL,
	auditcreatedby varchar(45) NOT NULL,
	auditcreateddate timestamp NOT NULL,
	auditlastmodifiedby varchar(45) NOT NULL,
	auditlastmodifieddate timestamp NOT NULL,
	auditsoftdelete varchar(3) NOT NULL
);
