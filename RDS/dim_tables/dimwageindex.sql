CREATE TABLE edw.dimwageindex (
	wageindexpk serial NOT NULL,
	cbsabk int4 NOT NULL,
	sourcefk int4 NOT NULL,
	wageindexnofloor numeric(20,10) NOT NULL,
	wageindex numeric(20,10) NOT NULL,
	auditeffectivestartdate date NOT NULL,
	auditeffectiveenddate date,
	auditcreatedby varchar(45) NOT NULL,
	auditcreateddate timestamp NOT NULL,
	auditlastmodifiedby varchar(45) NOT NULL,
	auditlastmodifieddate timestamp NOT NULL,
	auditcurrentrowidentifier char(3) NOT NULL,
	auditsoftdelete varchar(3) NOT NULL,
	PRIMARY KEY (wageindexpk)
);
