CREATE TABLE edw.dimsource (
	sourcepk serial NOT NULL,
	sourcedisplayname varchar(100) NOT NULL,
	sourcealternatename varchar(100),
	auditeffectivestartdate timestamp NOT NULL,
	auditeffectiveenddate timestamp,
	auditcreatedby varchar(45) NOT NULL,
	auditcreateddate timestamp NOT NULL,
	auditlastmodifiedby varchar(45) NOT NULL,
	auditlastmodifieddate timestamp NOT NULL,
	auditactiveindicator varchar(3) NOT NULL,
	auditsoftdelete varchar(3) NOT NULL,
	PRIMARY KEY (sourcepk)
);
