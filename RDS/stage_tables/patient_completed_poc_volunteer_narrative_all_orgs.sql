CREATE TABLE ods.patient_completed_poc_volunteer_narrative_all_orgs (
	orgid int8,
	chartid int4,
	activitytype varchar(500),
	assessmentid int4,
	volnarrativecreateddate timestamp,
	narrative varchar(500),
	insert_ts timestamp
);
