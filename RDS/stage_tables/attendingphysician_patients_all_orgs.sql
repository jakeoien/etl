CREATE TABLE ods.attendingphysician_patients_all_orgs (
	chartid int4,
	orgid int8,
	createddate timestamp,
	attendingphyscontactid varchar(500),
	npi varchar(500),
	nm_first varchar(500),
	nm_last varchar(500),
	insert_ts timestamp
);
