CREATE TABLE ods.patient_completed_poc_idgapproval_all_orgs (
	orgid int8,
	chartid int4,
	activitytype varchar(500),
	assessmentid int4,
	homecreateddate timestamp,
	signee varchar(500),
	signaturename varchar(500),
	eloctronicallysigning bit(1),
	signaturedate char(20),
	insert_ts timestamp
);
