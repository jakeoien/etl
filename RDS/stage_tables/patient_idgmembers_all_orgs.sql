CREATE TABLE ods.patient_idgmembers_all_orgs (
	orgid int8,
	chartid int4,
	"name" varchar(500),
	teammemberid varchar(500),
	teammember varchar(500),
	insert_ts timestamp
);
