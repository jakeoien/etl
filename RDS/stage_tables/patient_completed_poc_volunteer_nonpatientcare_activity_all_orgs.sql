CREATE TABLE ods.patient_completed_poc_volunteer_nonpatientcare_activity_all_orgs (
	orgid int8,
	chartid int4,
	activitytype varchar(500),
	assessmentid int4,
	volnpactivitycreateddate timestamp,
	acvtivity varchar(500),
	insert_ts timestamp
);
