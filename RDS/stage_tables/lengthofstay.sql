CREATE TABLE ods.lengthofstay (
	"id" int4,
	"_client_Id" int8,
	"name" varchar(500),
	ad_admit_date date,
	diagnosisname varchar(500),
	diagnosiscode varchar(500),
	los int4,
	insert_ts timestamp
);
