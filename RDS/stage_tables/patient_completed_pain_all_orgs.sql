CREATE TABLE ods.patient_completed_pain_all_orgs (
	orgid int8,
	chartid int4,
	"name" varchar(500),
	assessmentid int4,
	paincreateddate timestamp,
	painsectionid int4,
	assessment_with varchar(500),
	scheduled_opioid varchar(500),
	scheduled_opioid_init_cont varchar(500),
	scheduled_opioid_date timestamp,
	prn_opioid varchar(500),
	prn_opioid_init_cont varchar(500),
	prn_opioid_date timestamp,
	med_issue varchar(500),
	med_name varchar(500),
	medication_dosage int4,
	medication_frequency varchar(500),
	medication_route varchar(500),
	medication_concentration varchar(500),
	medication_administered_by varchar(500),
	medcreateddate timestamp,
	treatmentid int4,
	treatment_name varchar(500),
	treatment_issue varchar(500),
	treatment_frequency varchar(500),
	treatment_administered_by varchar(500),
	treatmentcreateddate timestamp,
	notes varchar(500),
	painsiteid int4,
	painsitecreateddate timestamp,
	painscale varchar(500),
	painlocation varchar(500),
	pain_type varchar(500),
	pain_score int4,
	pain_quality varchar(500),
	pain_level varchar(500),
	pain_frequency varchar(500),
	pain_duration varchar(500),
	pain_location_x varchar(500),
	pain_location_y varchar(500),
	pain_worse_better_ans varchar(500),
	pain_affect_patient_quality_ans varchar(500),
	pain_affect_ability_adl_ans varchar(500),
	insert_ts timestamp
);
