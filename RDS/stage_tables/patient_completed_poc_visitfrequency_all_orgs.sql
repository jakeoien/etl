CREATE TABLE ods.patient_completed_poc_visitfrequency_all_orgs (
	orgid int8,
	chartid int4,
	activitytype varchar(500),
	assessmentid int4,
	homecreateddate timestamp,
	visit varchar(500),
	frequency varchar(500),
	insert_ts timestamp
);
