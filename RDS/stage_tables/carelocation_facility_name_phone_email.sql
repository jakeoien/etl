CREATE TABLE ods.carelocation_facility_name_phone_email (
	"id" int4,
	orgid int8,
	facilitycreateddate timestamp,
	facilitylastupdateddate timestamp,
	facilityactive bit(1),
	facility_name varchar(500),
	facility_type int4,
	facilitycrmid int4,
	phone_type int4,
	primaryphone bit(1),
	"extension" varchar(500),
	"number" varchar(500),
	email_type int4,
	email varchar(500),
	primaryemail bit(1),
	insert_ts timestamp
);
