CREATE TABLE ods.patient_completed_pocgastrointestinal_issues (
	orgid int8,
	chartid int4,
	assessmentid int4,
	gastrosectionid int4,
	gastrocreateddate timestamp,
	issueid int4,
	issue_name varchar(500),
	issue_status varchar(500),
	"section" varchar(500),
	issuecreateddate timestamp,
	insert_ts timestamp
);
