CREATE TABLE ods.patient_completed_vital_all_orgs (
	orgid int8,
	chartid int4,
	"name" varchar(500),
	assessmentid int4,
	vitalscreateddate timestamp,
	temperature numeric(10,1),
	height numeric(10,2),
	bp varchar(500),
	weight numeric(10,1),
	heart_rate numeric(10,1),
	bmi numeric(10,1),
	respiratory numeric(10,1),
	defer_vital bit(1),
	muac varchar(500),
	orientation varchar(500),
	insert_ts timestamp
);
