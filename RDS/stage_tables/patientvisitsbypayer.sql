CREATE TABLE ods.patientvisitsbypayer (
	visitid int4,
	orgid int4,
	patientcrmid varchar(500),
	chart int4,
	visitdate timestamp,
	visitdiscipline varchar(500),
	visitactivitycode varchar(500),
	visitdurationinhours numeric(20,2),
	visitdurationinunits int4,
	traveltimeinhours numeric(20,2),
	patientpayerinfoid int4,
	payerid int4,
	payername varchar(500),
	payertype int4,
	payertypename varchar(500),
	grossrate numeric(20,2),
	netrate numeric(19,4)
);
