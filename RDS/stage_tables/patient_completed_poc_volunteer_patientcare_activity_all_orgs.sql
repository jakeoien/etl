CREATE TABLE ods.patient_completed_poc_volunteer_patientcare_activity_all_orgs (
	orgid int8,
	chartid int4,
	activitytype varchar(500),
	assessmentid int4,
	volpatientactivitycreateddate timestamp,
	activity varchar(500),
	insert_ts timestamp
);
