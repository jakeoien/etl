CREATE TABLE ods.patient_referral_aware_all_orgs (
	chartid int4,
	orgid int8,
	recordcreateddate timestamp,
	rf_aware varchar(500),
	insert_ts timestamp
);
