CREATE TABLE ods.carelocationfacilityinfo (
	"id" int4,
	orgid int4,
	facilitycreateddate timestamp,
	facilitylastupdateddate timestamp,
	facilityactive char(1),
	facility_name varchar(500),
	facility_type int4,
	facilitycrmid int4,
	phone_type int4,
	primaryphone char(1),
	"extension" varchar(500),
	"number" varchar(500),
	email_type int4,
	email varchar(500),
	primaryemail char(1),
	insert_ts timestamp
);
