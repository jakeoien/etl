CREATE TABLE ods.darepatients (
	orgid int8,
	assignedteam varchar(500),
	patientid int4,
	patientlastname varchar(500),
	patientfirstname varchar(500),
	patientmiddlename varchar(500),
	diagnosis varchar(500),
	admitdate date,
	dischargedate date,
	patientstatus varchar(500),
	insert_ts timestamp
);
