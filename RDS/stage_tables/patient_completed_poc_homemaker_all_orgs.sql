CREATE TABLE ods.patient_completed_poc_homemaker_all_orgs (
	orgid int8,
	chartid int4,
	activitytype varchar(500),
	assessmentid int4,
	homecreateddate timestamp,
	category varchar(500),
	subcategory varchar(500),
	status varchar(500),
	frequency varchar(500),
	insert_ts timestamp
);
