CREATE TABLE ods.patient_completed_bereavement_all_orgs (
	orgid int8,
	chartid int4,
	"name" varchar(500),
	assessmentid int4,
	bereavementcreateddate timestamp,
	bereavesectionid int4,
	loss_financial_security varchar(500),
	loss_financial_security_explain varchar(500),
	family_communication_style varchar(500),
	family_communication_style_explain varchar(500),
	bereavement_risk varchar(500),
	available_support varchar(500),
	pcg_name varchar(500),
	pcg_relation varchar(500),
	concurrent_losses varchar(500),
	expressedacceptance int4,
	opensupport int4,
	coping int4,
	appropriate_boundaries_score int4,
	totalbereavementrisk int4,
	prevalent_feelings varchar(500),
	notes varchar(500),
	insert_ts timestamp
);
