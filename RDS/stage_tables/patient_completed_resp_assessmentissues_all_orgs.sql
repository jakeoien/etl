CREATE TABLE ods.patient_completed_resp_assessmentissues_all_orgs (
	orgid int8,
	chartid int4,
	"name" varchar(500),
	assessmentsectionid int4,
	"_create_Date" timestamp,
	respsectionid int4,
	assessment_issue varchar(500),
	insert_ts timestamp
);
