CREATE TABLE ods.patient_completed_levelofcare_all_orgs (
	orgid int8,
	chartid int4,
	"name" varchar(500),
	"id" int4,
	loccreateddate timestamp,
	locsectionid int4,
	assessment_with varchar(500),
	cpr_discussion varchar(500),
	cpr_performed varchar(500),
	life_sustaining_treatments varchar(500),
	further_hospitalizations varchar(500),
	spiritual_concerns varchar(500),
	pre_exist_polst varchar(500),
	imm_of_death varchar(500),
	projected_bereave_risk varchar(500),
	med_regimen varchar(500),
	insert_ts timestamp
);
