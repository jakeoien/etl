CREATE TABLE ods.patient_completed_poc_hospiceaide_needs_all_orgs (
	orgid int8,
	chartid int4,
	activitytype varchar(500),
	assessmentid int4,
	needscreateddate timestamp,
	need varchar(500),
	insert_ts timestamp
);
