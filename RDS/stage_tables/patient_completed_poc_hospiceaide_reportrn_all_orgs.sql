CREATE TABLE ods.patient_completed_poc_hospiceaide_reportrn_all_orgs (
	orgid int8,
	chartid int4,
	activitytype varchar(500),
	assessmentid int4,
	rncreateddate timestamp,
	rn_parameter varchar(500),
	insert_ts timestamp
);
