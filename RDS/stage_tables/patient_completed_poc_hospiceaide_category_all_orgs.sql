CREATE TABLE ods.patient_completed_poc_hospiceaide_category_all_orgs (
	orgid int8,
	chartid int4,
	activitytype varchar(500),
	assessmentid int4,
	categorycreateddate timestamp,
	category varchar(500),
	subcategory varchar(500),
	status varchar(500),
	frequency varchar(500),
	insert_ts timestamp
);
