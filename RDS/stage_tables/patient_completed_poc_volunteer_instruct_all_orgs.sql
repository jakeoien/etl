CREATE TABLE ods.patient_completed_poc_volunteer_instruct_all_orgs (
	orgid int8,
	chartid int4,
	activitytype varchar(500),
	assessmentid int4,
	volinstructcreateddate timestamp,
	instruction varchar(500),
	frequency varchar(500),
	status varchar(500),
	insert_ts timestamp
);
