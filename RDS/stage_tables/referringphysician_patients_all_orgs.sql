CREATE TABLE ods.referringphysician_patients_all_orgs (
	chartid int4,
	orgid int8,
	createddate timestamp,
	refphyscontactid int4,
	npi varchar(500),
	nm_prefix varchar(500),
	nm_first varchar(500),
	nm_last varchar(500),
	nm_middle varchar(500),
	insert_ts timestamp
);
