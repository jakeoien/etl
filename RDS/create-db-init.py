import os

filenames = []
for filename in os.listdir("../RDS/dim_tables"):
    filenames.append("../RDS/dim_tables/" + filename)

for filename in os.listdir("../RDS/fact_tables"):
    filenames.append("../RDS/fact_tables/" + filename)

with open("../RDS/EDW-init.sql", "w") as edw_init_file:
    for filename in filenames:
        with open(filename, 'r') as readfile:
            for line in readfile:
                edw_init_file.write(line)
            edw_init_file.write("\n")

filenames = os.listdir("../RDS/stage_tables")

with open("../RDS/ODS-init.sql", "w") as ods_init_file:
    for filename in filenames:
        with open("../RDS/stage_tables/" + filename) as stage_file:
            ods_init_file.write(stage_file.read().replace("TABLE stg_", "TABLE ods."))
        ods_init_file.write("\n")
