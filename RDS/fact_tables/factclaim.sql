CREATE TABLE edw.factclaim (
	claimpk int4 NOT NULL,
	claimdatefk int4 NOT NULL,
	claimstatusfk int4 NOT NULL,
	organizationfk int4,
	individualfk int4 NOT NULL,
	payerfk int4 NOT NULL,
	payertypefk int4,
	benefitfk int4,
	billtypefk int4,
	sourcefk int4 NOT NULL,
	grossamount numeric(19,4) NOT NULL,
	netamount numeric(19,4) NOT NULL,
	auditcreatedby varchar(45) NOT NULL,
	auditcreateddate timestamp NOT NULL,
	auditlastmodifiedby varchar(45) NOT NULL,
	auditlastmodifieddate timestamp NOT NULL,
	auditsoftdelete varchar(3) NOT NULL
);
