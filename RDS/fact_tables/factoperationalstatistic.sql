CREATE TABLE edw.factoperationalstatistic (
	dateofoperationfk int4 NOT NULL,
	patientfk int4 NOT NULL,
	facilityfk int4 NOT NULL,
	levelofcarefk int4 NOT NULL,
	payertypefk int4 NOT NULL,
	diagnosisfk int4 NOT NULL,
	referralsourcefk int4 NOT NULL,
	periodstartdatefk int4 NOT NULL,
	periodenddatefk int4 NOT NULL,
	countreferrals int4,
	countadmissions int4,
	countactualadmissions int4,
	countconverted int4,
	countdischarge int4,
	countactualdischarge int4,
	countreadmit int4,
	countvisit int4,
	countmissedvisit int4,
	countexpectedvisit int4,
	sumtotalvisittimeinminutes numeric(10,5),
	sumtotalvisittimeinhours numeric(10,5),
	sumtraveltimeinminutes int4,
	sumtraveltimeinhours int4,
	sumtravelcost numeric(19,4),
	sumpatientservicedays int4,
	sumexpectedpatientservicedays int4,
	sumlengthofstaydays int4,
	sumexpectedlengthofstaydays int4,
	sumdailyrevenue numeric(19,4),
	sumlaborshare numeric(19,4),
	qualitydatapenalty varchar(3),
	sumqualitydatapenaltycost char(10),
	sumlaborhoursamount numeric(19,4),
	sumexpectedlaborhoursamount numeric(19,4),
	sumsalaries numeric(19,4),
	sumexpectedsalaries numeric(19,4),
	sumbenefits numeric(19,4),
	sumexpectedbenefits numeric(19,4),
	sumtotallaborcost numeric(19,4),
	sumexpectedtotallaborcost numeric(19,4),
	sumbenefitsppd numeric(19,4),
	sumexpectedbenefitsppd numeric(19,4),
	sumexpectedoperatingexpense numeric(19,4),
	sumactualoperatingexpense numeric(19,4),
	sumexpectedebitda numeric(19,4),
	sumactualebitda numeric(19,4),
	auditcreatedby varchar(45) NOT NULL,
	auditcreateddate timestamp NOT NULL,
	auditlastmodifiedby varchar(45) NOT NULL,
	auditlastmodifieddate timestamp NOT NULL,
	auditsoftdelete varchar(3) NOT NULL
);
