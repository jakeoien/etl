#!/usr/bin/env bash

sudo yum update -y

sudo pip install --upgrade pip
export PATH="/usr/local/:$PATH"

sudo yum install postgresql postgresql-server postgresql-devel postgresql-contrib postgresql-docs -y
sudo service postgresql initdb
# possibly need to edit config files like on http://imperialwicket.com/aws-install-postgresql-on-amazon-linux-quick-and-dirty/
sudo service postgresql start

wget https://www.python.org/ftp/python/3.6.2/Python-3.6.2.tgz
tar zxvf Python-3.6.2.tgz
cd Python-3.6.2
sudo yum install gcc
./configure --prefix=/opt/python3
make
sudo yum install openssl-devel -y
sudo make install
sudo ln -s /opt/python3/bin/python3 /usr/bin/python3
sudo ln -s /opt/python3/bin/pip3 /usr/bin/pip3

sudo pip3 install psycopg2
sudo pip3 install boto3