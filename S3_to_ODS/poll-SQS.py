import boto3
from datetime import datetime
import json
import psycopg2
import os
import csv
import io


def create_sql_query_from_csv(table_name, row_dict, insert_timestamp=datetime.now().strftime("%Y-%m-%d %H:%M:00")):
    """Create a SQL query from the table name and row data, including an additional timestamp inserted field

    :param str table_name: Name of the table to insert into
    :param dict row_dict: Dictionary containing the row data in the format: {"col1": value, "col2": value, etc}
    :param str insert_timestamp: Timestamp of the format 'YYYY-MM-DD hh:mm:00'

    :return: the fully assembled query
    :rtype: str
    """
    col_names_str = ""
    values_str = ""

    col_names_in_query = []  # some columns are blank and won't be included in the query

    # figure out which columns to include - some may have no data, make those null by not including them in the query
    for col_name in row_dict:
        if row_dict[col_name] == "":
            continue
        col_names_in_query.append(col_name)

    # build the query
    for col_name in col_names_in_query:
        col_names_str += col_name + ", "
        values_str += "'{}', ".format(row_dict[col_name].replace("'", "''"))  # double up single quotes to preserve them
    col_names_str += "insert_ts"  # include the timestamp data at the end of the query
    values_str += f"'{insert_timestamp}'"

    col_names_str = col_names_str.lower()  # the ODS tables are all lowercase versions of the viewnames

    # return the assembled query
    return f"INSERT INTO {table_name} ({col_names_str}) VALUES ({values_str})"


sqs = boto3.client("sqs")
""":type : pyboto3.sqs"""
queue_name = "S3_Messages"

try:
    queue_url = sqs.get_queue_url(QueueName=queue_name)["QueueUrl"]
except Exception as e:
    print(f"Could not get queue url for queue {queue_name}; it is likely that queue does not exist.")
    exit()  # todo replace with send SNS notification

# get a message from the queue
message_response = sqs.receive_message(QueueUrl=queue_url, WaitTimeSeconds=5)  # todo configure retry policy
print(message_response)
if message_response.get("Messages", None):
    message_body = json.loads(message_response["Messages"][0]["Body"])
    bucket = message_body["Bucket"]
    key = message_body["Key"]
    insert_timestamp = message_body["InsertTimestamp"]
    table_name = message_body["TableName"]
else:
    print("No messages in queue")
    exit()  # todo decide if this function will constantly be polling for messages or be triggered by Lambda
print(f"Got message: {key} was uploaded to {bucket}. Inserting into {table_name} with timestamp {insert_timestamp}")

s3 = boto3.client("s3")
""":type : pyboto3.s3"""
try:
    print(f"Downloading s3://{bucket}/{key}")
    response = s3.get_object(Bucket=bucket, Key=key)  # todo investigate timeouts on objects which don't exist
    file_contents = response["Body"].read().decode("utf-8")
    print("Downloaded file")
except Exception as ex:
    print(ex)
    print(f"Error getting object {key} from bucket {bucket}.")  # todo retry a couple of times, then send failure SNS notification
    raise ex

# Read the file just downloaded

host = "etldb.czffvv5a9z6m.us-east-1.rds.amazonaws.com"
port = "5432"
database = "etl"
user = "etl_master"
password = os.getenv("etl_password")  # todo figure out encryption - this is for version control purposes only

print(f"Connecting to {host}")
try:
    connection = psycopg2.connect(host=host, port=port, database=database, user=user, password=password)
except Exception as e:
    print("Connection failed")
    print(e.args)
    print(e)
    raise e  # todo send SNS if DB connection fails
else:
    print("Connection successful")

failed_rows = {}

with connection:
    print("Working on " + key)
    cursor = connection.cursor()
    failed_rows[table_name] = []
    reader = csv.DictReader(io.StringIO(file_contents), delimiter="|")

    # Generate a sql query for each row in the csv file
    for row_num, row in enumerate(reader):
        query = create_sql_query_from_csv(table_name, row, insert_timestamp)
        try:
            cursor.execute(query)
        except psycopg2.IntegrityError as ie:
            print(f"IntegrityError on row {row_num}: {ie}")
            failed_rows[table_name].append({"rowData": row, "error": ie.pgerror, "rowNum": row_num})
        except psycopg2.DataError as de:
            print(f"DataError on row {row_num}: {de}")
            failed_rows[table_name].append({"rowData": row, "error": de.pgerror, "rowNum": row_num})
        except psycopg2.ProgrammingError as pe:
            print(f"ProgrammingError on row {row_num}: {pe}")
            failed_rows[table_name].append({"rowData": row, "error": pe.pgerror, "rowNum": row_num})

    if failed_rows[table_name] == []:
        print("No failed rows")
        connection.commit()
        function_status = "Success"  # todo remove message from the queue
        sqs.delete_message(QueueUrl=queue_url, ReceiptHandle=message_response["Messages"][0]["ReceiptHandle"])
    else:
        print("Some rows failed to be inserted, see the Lambda return value for the reason why")
        connection.rollback()
        function_status = "Failure"
        sqs.change_message_visibility(QueueUrl=queue_url, ReceiptHandle=message_response["Messages"][0]["ReceiptHandle"], VisibilityTimeout=0)

    with open("job_status.txt", "w") as file:
        json.dumps({"jobStatus": "Success", "failedRows": []})  # todo change to add SNS notification for success
