import boto3
import json
import re

s3 = boto3.client("s3", endpoint_url="http://localhost:4572")
""":type : pyboto3.s3"""


s3.download_fileobj(Fileobj=str, Bucket="curantis-csv-dumps", Key="text.txt")

print(str)

exit()

types_of_errors = set()

with open("failed_rows.txt", 'r') as file:
    obj = file.read()

    failed_rows = json.loads(obj)

    for table_name in failed_rows:
        if failed_rows[table_name]:
            for row in failed_rows[table_name]:
                error = row["error"]
                row_data = row["row"]

                if "too long" in error:
                    print(error)

                    regex = re.search(r'.*varying\((\d+)\)', error)
                    length_of_error = int(regex.group(1))

                    for column_name in row_data:
                        if len(row_data[column_name]) > length_of_error:
                            print("Table name: {}\nSupposed Length: {}\nColumn name: {}\nValue: {}\n\n"
                                  .format(table_name, length_of_error, column_name, row_data[column_name]))
                    break


                types_of_errors.add(error)
    for error in types_of_errors:
        print(error)



