import urllib.parse
import boto3
import re
import json

# This function will drop a message into an SQS queue when a file is loaded into an S3 bucket
s3_queue_name = "S3_Messages"
sqs = boto3.client("sqs")
""":type : pyboto3.sqs"""


def handler(event, context):
    """Handles the trigger initiated by the S3 bucket"""
    bucket = event['Records'][0]['s3']['bucket']['name']  # name of S3 bucket that triggered the event
    key = urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'], encoding="utf-8")  # name of file that triggered the event
    # expecting a format of YYYYMMddhhmm_[REP_v]|[Ent_]<viewname>.csv

    print(f"Triggered by S3 event: {key} was uploaded to {bucket}")

    # extract the date and name of table, for use in creating SQL query, stripping common prefixes in the table name
    search_regex = re.search('(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})_(?:(?:REP_v)|(?:[Ee]nt_))?(.*)\.csv', key)
    if search_regex:
        insert_timestamp = "{}-{}-{} {}:{}:00".format(*search_regex.group(1, 2, 3, 4, 5))  # get date values from regex
        table_name = f"ods.{search_regex.group(6).lower()}"  # ods.table_name
    else:
        print("Invalid file name - Expecting YYYYMMddhhmm_[REP_v]|[Ent_]<viewname>.csv")
        print("Exiting")
        exit()  # todo send notifications on function failure here

    print(f"Table name: {table_name}")
    print(f"Insert timestamp: {insert_timestamp}")
    # todo send message to SQS queue

    message_body = {
        "Key": key,
        "Bucket": bucket,
        "TableName": table_name,
        "InsertTimestamp": insert_timestamp
    }

    try:
        queue_url = sqs.get_queue_url(QueueName=s3_queue_name)["QueueUrl"]
    except Exception:
        print(f"Could not get queue url for queue {s3_queue_name}; it is likely that queue does not exist.")
        exit()  # todo replace with send SNS notification

    sqs.send_message(QueueUrl=queue_url, MessageBody=json.dumps(message_body))
    return "Success"
